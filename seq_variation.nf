include { whatshap } from '../whatshap/whatshap.nf'
include { whatshap_tumor_by_chr } from '../whatshap/whatshap.nf'
include { whatshap_stats } from '../whatshap/whatshap.nf'

include { extract_chroms_from_gff } from '../ref_utils/ref_utils.nf'

include { bcftools_view } from '../bcftools/bcftools.nf'
include { bcftools_reheader as bcftools_reheader_somatic } from '../bcftools/bcftools.nf'
include { bcftools_reheader as bcftools_reheader_germline } from '../bcftools/bcftools.nf'
include { bcftools_concat } from '../bcftools/bcftools.nf'
include { bcftools_concat_phased } from '../bcftools/bcftools.nf'

include { bcftools_index_somatic } from '../bcftools/bcftools.nf'
include { bcftools_index_somatic as bcftools_index_germline } from '../bcftools/bcftools.nf'
include { bcftools_index_somatic as bcftools_index_phased_germline } from '../bcftools/bcftools.nf'
include { bcftools_index_somatic as bcftools_index_tumor } from '../bcftools/bcftools.nf'
include { bcftools_index_somatic as bcftools_index_phased_tumor } from '../bcftools/bcftools.nf'
include { htslib_bgzip_somatic } from '../htslib/htslib.nf'
include { htslib_bgzip_somatic as htslib_bgzip_germline} from '../htslib/htslib.nf'
include { htslib_bgzip_somatic as htslib_bgzip_phased_germline} from '../htslib/htslib.nf'
include { htslib_bgzip_somatic as htslib_bgzip_tumor } from '../htslib/htslib.nf'
include { htslib_bgzip_somatic as htslib_bgzip_phased_tumor} from '../htslib/htslib.nf'

include { jacquard_merge_tumor } from '../jacquard/jacquard.nf'

include { lenstools_get_expressed_transcripts_bed } from '../lenstools/lenstools.nf'
include { lenstools_get_fusion_transcripts_bed } from '../lenstools/lenstools.nf'
include { samtools_faidx_fetch_somatic_folder } from '../samtools/samtools.nf'
include { samtools_faidx_fetch } from '../samtools/samtools.nf'

include { samtools_faidx } from '../samtools/samtools.nf'

include { lenstools_make_variant_specific_vcfs } from '../lenstools/lenstools.nf'
include { lenstools_make_variant_specific_tx_seqs } from '../lenstools/lenstools.nf'
include { starfusion_fusions_to_fusion_txs } from '../fusion/fusion.nf'

include { lenstools_make_fusion_specific_tx_seqs } from '../lenstools/lenstools.nf'

workflow tx_list_to_norm_and_tumor_cds_fastas {
// Given a set of somatic variants, set of expressed transcripts, a reference
// FASTA, a GTF, and phased VCFs, do the following:
// Make BED with exonic regions expressed somatic-variant harboring transcripts.
//         |
//         V
// Create per-somatic variants VCFs that contain variant of interest and
// neighboring phased variants and homozygous variants.
//         |
//         V
// Use exonic BED file with reference FASTA and samtools faidx fetch to
// generate exon-level FASTAs.
//         |
//         V
// Use bcftools consensus to create variant-specific transcript sequences that
// include the variant of interest and any phased/homozygous variants.
//
// Note: This workflow is, for better or worse, quite rigid due to the nature
// of tools used. It also generate the normal transcript (with germline
// variants) for calculating agretopicity. LENS has shifted towards using a
// blastp-based strategy for calculating agretopicity, but these normal
// transcripts may still be useful in the future.
  take:
    somatic_vcfs
    tx_lists
    gtf
    dna_ref
    normal_phased_vcfs
    tumor_phased_vcfs
    bcftools_index_phased_germline_parameters
    bcftools_index_phased_tumor_parameters
    lenstools_get_expressed_transcripts_bed_parameters
    samtools_faidx_fetch_somatic_folder_parameters
  main:
    htslib_bgzip_phased_germline(
      normal_phased_vcfs)
    bcftools_index_phased_germline(
      htslib_bgzip_phased_germline.out.bgzip_files,
      bcftools_index_phased_germline_parameters)
    htslib_bgzip_phased_tumor(
      tumor_phased_vcfs)
    bcftools_index_phased_tumor(
      htslib_bgzip_phased_tumor.out.bgzip_files,
      bcftools_index_phased_tumor_parameters)
    lenstools_get_expressed_transcripts_bed(
      tx_lists,
      gtf,
      lenstools_get_expressed_transcripts_bed_parameters)
    somatic_vcfs
      .join(tumor_phased_vcfs, by: [0, 1, 2, 3])
      .join(normal_phased_vcfs, by: [0])
      .map{ [it[0], it[1], it[2], it[3], it[4], it[9], it[5]] }
      .set{ somatic_w_norm_w_tumor }
    lenstools_make_variant_specific_vcfs(
      somatic_w_norm_w_tumor)
    samtools_faidx_fetch_somatic_folder(
      lenstools_get_expressed_transcripts_bed.out.expressed_transcripts_beds,
      dna_ref,
      'expressed_txs',
      samtools_faidx_fetch_somatic_folder_parameters)
    lenstools_make_variant_specific_vcfs.out.somatic_vcf_and_vcf_spec_vcfs
      .join(samtools_faidx_fetch_somatic_folder.out.fetched_fastas, by: [0, 1, 2, 3])
      .set{ var_vcfs_and_tx_seqs }
    lenstools_make_variant_specific_tx_seqs(
      var_vcfs_and_tx_seqs)
  emit:
    somatic_vcfs_w_var_tx_seqs = lenstools_make_variant_specific_tx_seqs.out.somatic_vcfs_w_var_tx_seqs
}


workflow make_phased_germline_vars {
// Given an indexed, filtered germline VCF, relevant normal DNA BAMs, a
// reference FASTA, and a GTF, phased the germline variants.
  take:
    germ_vars_and_idxs
    dna_bams_and_bais
    aln_ref
    gtf
    var_phaser_tool
    var_phaser_tool_parameters
  main:
    dna_bams_and_bais
      .filter{ it[1] =~ /nd-/ }
      .set{ germ_dna_bams_and_bais }

    germ_vars_and_idxs
      .join(germ_dna_bams_and_bais, by: [0,1,2])
      .map{ [it[0], it[1], 'NA', it[2], it[3], it[4], it[5], it[6]] }
      .set{ concatd_vcfs_and_dna_bams }

    phased_vcfs = Channel.empty()

    var_phaser_tool_parameters = Eval.me(var_phaser_tool_parameters)
    if( var_phaser_tool =~ /whatshap/ ) {
      whatshap_parameters = var_phaser_tool_parameters['whatshap'] ? var_phaser_tool_parameters['whatshap'] : '--ignore-read-groups'
      whatshap_stats_parameters = var_phaser_tool_parameters['whatshap_stats'] ? var_phaser_tool_parameters['whatshap_stats'] : ''
      samtools_faidx(
        aln_ref,
        '')
      whatshap(
        concatd_vcfs_and_dna_bams,
        samtools_faidx.out.faidx_file,
        whatshap_parameters)
      whatshap_stats(
        whatshap.out.phased_vcfs,
        whatshap_stats_parameters)
      whatshap.out.phased_vcfs
        .set{ phased_vcfs }
    }
    if( var_phaser_tool =~ /haplotypetools/ ) {
      haplotypetools_parameters = var_phaser_tool_parameters['haplotypetools'] ? var_phaser_tool_parameters['haplotypetools'] : ''
      haplotypetools_stats_parameters = var_phaser_tool_parameters['haplotypetools_stats'] ? var_phaser_tool_parameters['haplotypetools_stats'] : ''
      samtools_faidx(
        aln_ref,
        '')
      haplotypetools(
        concatd_vcfs_and_dna_bams,
        samtools_faidx.out.faidx_file,
        haplotypetools_parameters)
      haplotypetools
        .set{ phased_vcfs }
    }
  emit:
    phased_vcfs
}


workflow make_phased_tumor_vars {
// require:
//   GERM_VARS
//   SOM_VARS
//   DNA_BAMS_AND_BAIS
//   RNA_BAMS_AND_BAIS
//   params$seq_variation$make_phased_tumor_vars$aln_ref
//   params$seq_variation$make_phased_tumor_vars$gtf
  take:
    tumor_vars_and_idxs
    dna_bams_and_bais
    rna_bams_and_bais
    aln_ref
    gtf
    species
    var_phaser_tool
    var_phaser_tool_parameters
  main:
    if( species =~ /[Hh]uman|hs|HS|[Hh]omo/ ) {
        chrom_count = 25
    } else if( species =~ /[Mm]ouse|mm|MM|[Mm]us/ ) {
        chrom_count = 21
    }
    extract_chroms_from_gff(
      gtf)

    dna_bams_and_bais
      .filter{ it[1] =~ /ad-/ }
      .set{ tumor_dna_bams_and_bais}

    tumor_vars_and_idxs
      .join(tumor_dna_bams_and_bais, by: [0])
      .set{ concatd_vcfs_and_dna_bams }
    concatd_vcfs_and_dna_bams.join(rna_bams_and_bais, by: 0)
      .map{ [it[0], it[1], it[2], it[10], it[3], it[4], it[5], it[8], it[9], it[12], it[13]] }
      .set{ concatd_vcfs_and_dna_bams_and_rna_bams }

    var_phaser_tool_parameters = Eval.me(var_phaser_tool_parameters)
    if( var_phaser_tool =~ /whatshap/ ) {
      whatshap_parameters = var_phaser_tool_parameters['whatshap'] ? var_phaser_tool_parameters['whatshap'] : ''
      whatshap_stats_parameters = var_phaser_tool_parameters['whatshap_stats'] ? var_phaser_tool_parameters['whatshap_stats'] : ''
      concatd_vcfs_and_dna_bams_and_rna_bams
        .combine(extract_chroms_from_gff.out.chroms_list.splitText())
        .set{ phase_inputs_w_chr }
      samtools_faidx(
        aln_ref,
        '')
      whatshap_tumor_by_chr(
        phase_inputs_w_chr,
        samtools_faidx.out.faidx_file,
        whatshap_parameters) //'--ignore-read-groups'
      whatshap_stats(
        whatshap_tumor_by_chr.out.phased_vcfs,
        whatshap_stats_parameters)
      whatshap_tumor_by_chr.out.phased_vcfs
        .groupTuple(by: [0, 1, 2, 3], size: chrom_count)
        .set{ phased_vcfs_by_patient }
      bcftools_concat_phased(
        phased_vcfs_by_patient,
        '-a',
        'phased')
      bcftools_concat_phased.out.concatd_vcfs
        .set{ phased_vcfs }
    }
    if( var_phaser_tool =~ /haplotypetools/ ) {
      haplotypetools_parameters = var_phaser_tool_parameters['haplotypetools'] ? var_phaser_tool_parameters['haplotypetools'] : ''
      haplotypetools_stats_parameters = var_phaser_tool_parameters['haplotypetools_stats'] ? var_phaser_tool_parameters['haplotypetools_stats'] : ''
      samtools_faidx(
        aln_ref,
        '')
      haplotypetools_tumor(
        concatd_vcfs_and_dna_bams,
        samtools_faidx.out.faidx_file,
        haplotypetools_parameters)
      haplotypetools_tumor.out.phased_vcfs
        .set{ phased_vcfs }
    }
  emit:
    phased_vcfs
}


workflow germ_and_som_vars_to_tumor_vars {
// require:
//   GERM_VARS
//   SOM_VARS
//   params$seq_variation$germ_and_some_vars_to_tumor_vars$vcf_merge_tool
//   params$seq_variation$germ_and_some_vars_to_tumor_vars$vcf_merge_tool_parameters
  take:
    germ_vars_and_csis
    som_vars_and_csis
    vcf_merge_tool
    vcf_merge_tool_parameters
  main:
    vcf_merge_tool_parameters = Eval.me(vcf_merge_tool_parameters)
    // Typically, parameters are extracted _after_ the if conditionals, but
    // these tools are used in both supported cases, so they're being extracted
    // here.
    bcftools_view_parameters = vcf_merge_tool_parameters['bcftools_view'] ? vcf_merge_tool_parameters['bcftools_view'] : ''
    bcftools_reheader_somatic_parameters = vcf_merge_tool_parameters['bcftools_reheader_somatic'] ? vcf_merge_tool_parameters['bcftools_reheader_somatic'] : ''
    bcftools_index_somatic_parameters = vcf_merge_tool_parameters['bcftools_index_somatic'] ? vcf_merge_tool_parameters['bcftools_index_somatic'] : ''
    bcftools_reheader_germline_parameters = vcf_merge_tool_parameters['bcftools_reheader_germline'] ? vcf_merge_tool_parameters['bcftools_reheader_germline'] : ''
    bcftools_index_germline_parameters = vcf_merge_tool_parameters['bcftools_index_germline'] ? vcf_merge_tool_parameters['bcftools_index_germline'] : ''
    bcftools_index_tumor_parameters = vcf_merge_tool_parameters['bcftools_index_tumor'] ? vcf_merge_tool_parameters['bcftools_index_tumor'] : ''

    germ_vars_and_csis
      .join(som_vars_and_csis.map{ [it[0], it[1], it[3], it[4], it[2]]}, by: [0, 1, 2])
      .set{ germ_vars_and_csis_w_tum_id }

    merged_vcfs = Channel.empty()

    if( vcf_merge_tool =~ /bcftools/ ) {
      bcftools_concat_parameters = vcf_merge_tool_parameters['bcftools_concat'] ? vcf_merge_tool_parameters['bcftools_concat'] : '-a'
      bcftools_view(
        som_vars_and_csis,
        'somatic_only',
        bcftools_view_parameters)
      htslib_bgzip_somatic(
        bcftools_view.out.viewed_vcfs)
      bcftools_reheader_somatic(
        htslib_bgzip_somatic.out.bgzip_files,
        bcftools_reheader_somatic_parameters)
      bcftools_index_somatic(
        bcftools_reheader_somatic.out.reheadered_vcfs,
        bcftools_index_somatic_parameters)
      bcftools_reheader_germline(
          germ_vars_and_csis_w_tum_id.map{ [it[0], it[1], it[6], it[2], it[3] ]},
        bcftools_reheader_germline_parameters)
      bcftools_index_germline(
        bcftools_reheader_germline.out.reheadered_vcfs,
        bcftools_index_germline_parameters)
      bcftools_index_somatic.out.vcfs_w_csis
        .join(bcftools_index_germline.out.vcfs_w_csis, by: [0, 1, 2, 3])
        .map{ [it[0], it[1], it[2], it[3], [it[4], it[6]], [it[5], it[7]]] }
        .set{ somatic_and_germline_vcfs_w_csis }
      normalize_vcf_formats(
        somatic_and_germline_vcfs_w_csis)
      bcftools_concat(
        normalize_vcf_formats.out.reformed_vcfs,
        bcftools_concat_parameters, //'-a',
        'tumor')
      bcftools_concat.out.concatd_vcfs
        .set{ merged_vcfs }
      }
    if( vcf_merge_tool =~ /jacquard_merge/ ) {
      jacquard_merge_parameters = vcf_merge_tool_parameters['jacquard_merge'] ? vcf_merge_tool_parameters['jacquard_merge'] : "--include_format_tags='GT' --include_rows='all'"
      bcftools_view(
        som_vars_and_csis,
        'somatic_only',
        bcftools_view_parameters)
      htslib_bgzip_somatic(
        bcftools_view.out.viewed_vcfs)
      bcftools_reheader_somatic(
        htslib_bgzip_somatic.out.bgzip_files,
        bcftools_reheader_somatic_parameters)
      bcftools_index_somatic(
        bcftools_reheader_somatic.out.reheadered_vcfs,
        bcftools_index_somatic_parameters)
      bcftools_reheader_germline(
          germ_vars_and_csis_w_tum_id.map{ [it[0], it[1], it[6], it[2], it[3] ]},
        bcftools_reheader_germline_parameters)
      bcftools_index_germline(
        bcftools_reheader_germline.out.reheadered_vcfs,
        bcftools_index_germline_parameters)
      bcftools_index_somatic.out.vcfs_w_csis
        .join(bcftools_index_germline.out.vcfs_w_csis, by: [0, 1, 2, 3])
        .map{ [it[0], it[1], it[2], it[3], [it[4], it[6]], [it[5], it[7]]] }
        .set{ somatic_and_germline_vcfs_w_csis }
      normalize_vcf_formats(
        somatic_and_germline_vcfs_w_csis)
      jacquard_merge_tumor(
        normalize_vcf_formats.out.reformed_vcfs,
        jacquard_merge_parameters)
      jacquard_merge_tumor.out.concatd_vcfs
        .set{ merged_vcfs }
      }
    htslib_bgzip_tumor(
      merged_vcfs)
    bcftools_index_tumor(
      htslib_bgzip_tumor.out.bgzip_files,
      bcftools_index_tumor_parameters)
  emit:
      tumor_vars = merged_vcfs
      tumor_vars_and_idxs = bcftools_index_tumor.out.vcfs_w_csis
}


workflow fusions_to_fusion_cds_fastas {
  take:
    fusions
    gtf
    dna_ref
    normal_phased_vcfs
    bcftools_index_phased_germline_parameters
    lenstools_get_fusion_transcripts_bed_parameters
    samtools_faidx_fetch_parameters
  main:
    htslib_bgzip_phased_germline(
      normal_phased_vcfs)
    bcftools_index_phased_germline(
      htslib_bgzip_phased_germline.out.bgzip_files,
      bcftools_index_phased_germline_parameters)
    bcftools_index_phased_germline.out.vcfs_w_csis
      .map{ [it[0], it[1], it[3], it[4], it[5]] }
      .set{ normal_phased_vcfs_trunc }
    fusions.combine(normal_phased_vcfs_trunc, by: [0, 2])
      .map{ [it[0], it[2], it[4], it[1], it[3], it[5], it[6]] }
      .set{ fusions_w_normal_phased_vcfs }
    starfusion_fusions_to_fusion_txs(
      fusions)
    lenstools_get_fusion_transcripts_bed(
      starfusion_fusions_to_fusion_txs.out.fusion_transcripts,
      gtf,
      lenstools_get_fusion_transcripts_bed_parameters)
    samtools_faidx_fetch(
      lenstools_get_fusion_transcripts_bed.out.fusion_transcripts_beds,
      dna_ref,
      'expressed_txs',
      samtools_faidx_fetch_parameters)
    samtools_faidx_fetch.out.fetched_fastas
      .combine(normal_phased_vcfs_trunc, by: [0, 2])
      .map{ [it[0], it[2], it[4], it[1], it[3], it[5], it[6]] }
      .set{ exon_fas_w_normal_phased_vcfs }
    lenstools_make_fusion_specific_tx_seqs(
      exon_fas_w_normal_phased_vcfs)
  emit:
      fusion_tx_exons = lenstools_make_fusion_specific_tx_seqs.out.var_tx_seqs
}


process normalize_vcf_formats {
// Effectively a pass through script as I believe Jacquard will handle this
// functionality. Look into remove this in the future.

  tag "${dataset}/${pat_name}/${norm_run}_${tumor_run}"
  label 'bcftools_container'

  input:
  tuple val(pat_name), val(norm_run), val(tumor_run), val(dataset), path(vcfs), path(csis)

  output:
  tuple val(pat_name), val(norm_run), val(tumor_run), val(dataset), path('*af.vcf.gz'), path('*csi'), emit: reformed_vcfs

  script:
  """
  mkdir -p normed_vcfs
  for i in *vcf.gz; do
    echo \${i}
    zcat \${i} | sed 's/ID=VAF/ID=AF/g' | sed 's/:VAF/:AF/g' | bgzip > normed_vcfs/\${i%.vcf.gz}.af.vcf.gz
  done

  mkdir -p old_vcfs
  mv *vcf.gz old_vcfs
  mv *vcf.gz.csi old_vcfs
  mv normed_vcfs/* .

  for j in *vcf.gz;
    do bcftools index \${j}
  done
  """
}
